package ideologyllc.com.encryptiontest;

/**
 * Created by ${Arif} on 11/12/2015.
 */
public class Cryptograph {

    private static final int[] chain = {1224, 1990, 1988, 2015, 4458, 2020};

    /**
     *
     * @param key
     * @return encrypted message
     */
    public static String encrypt(String key){

        String result = "";
        int l = key.length();
        char ch;
        int currentKey = 0;
        for (int i = 0; i<l; i++){
            if (currentKey > chain.length -1 ){
                currentKey = 0;
            }

            ch = key.charAt(i);
            ch += chain[currentKey];
            result += ch;
            currentKey++;
        }
        return result;
    }

    /**
     *
     * @param key
     * @return decrypted message
     */
    public static String decrypt(String key){

        String result = "";
        int l = key.length();
        char ch;
        int currentKey = 0;
        for (int i = 0; i<l; i++){
            if (currentKey > chain.length -1 ){
                currentKey = 0;
            }

            ch = key.charAt(i);
            ch -= chain[currentKey];
            result += ch;
            currentKey++;
        }
        return result;
    }
}
